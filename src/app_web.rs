use makepad_widgets::*;

use crate::data::state::State;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;
    import crate::home::home_screen::HomeScreen;
    import crate::aboutus::about_us::AboutUs;

    import crate::shared::widgets::NavbarMenuButton;

    ICON_DISCOVER = dep("crate://self/resources/icons/discover.svg")
    ICON_CHAT = dep("crate://self/resources/icons/chat.svg")
    ICON_MY_MODELS = dep("crate://self/resources/icons/my_models.svg")

    AppWebUI = <ScrollXYView>{
        width: Fill,
        height: Fill,
        align: {x: 0.5},
        padding: {top: 0, bottom: 48 }
        // debug: A

        navbar_menu = <RoundedView> {
            width: Fill,
            height: Fit,
            flow: Right, spacing: 20.0,
            padding: { top: 0 }
            align: {x: 0.5, y: 0.0},

            show_bg: true,
            draw_bg: {
                color: (SIDEBAR_BG_COLOR),
                instance radius: 0.0,
                border_color: #EAECF0,
                border_width: 1.2,
            }
            home_screen_tab = <NavbarMenuButton> {
                animator: {selected = {default: on}}
                text: "Home",
                draw_icon: {
                    svg_file: (ICON_DISCOVER),
                }
            }
            aboutus_screen_tab = <NavbarMenuButton> {
                text: "About Us",
                draw_icon: {
                    svg_file: (ICON_MY_MODELS),
                }
            }
            application_pages = <View> {
                margin: 0.0,
                padding: 0.0,
                flow: Down,
                width: Fill,
                height: Fill,
                // debug: A

                home_screen_frame = <HomeScreen> {align: {x: 0.5, y: 0.5}, visible: true}
                aboutus_screen_frame = <AboutUs> {visible: false}
            }
        }
    }
}
#[derive(Live)]
pub struct AppWebUI {
    #[live]
    view: WidgetRef,
}

impl LiveRegister for AppWebUI {
    fn live_register(cx: &mut Cx) {
        crate::makepad_widgets::live_design(cx);
        crate::app_ui::live_design(cx);
        crate::app_mobile::live_design(cx);
        // crate::app_web::live_design(cx);

        crate::home::live_design(cx);
        crate::aboutus::live_design(cx);
        crate::shared::live_design(cx);
    }
}
impl LiveHook for AppWebUI {
    fn after_new_from_doc(&mut self, _cx: &mut Cx) {
        println!("after_new_from_doc(): starting some kind of a loop");
    }
}

impl MatchEvent for AppWebUI {
    fn handle_draw_2d(&mut self, cx: &mut Cx2d) {
        let navbar_menu = self.view.view(id!(navbar_menu));
        while let Some(next) = self.view.draw(cx, &mut Scope::empty()).step() {
            navbar_menu.draw_all(cx, &mut Scope::empty());
        }
    }
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions) {
        self.view
            .radio_button_set(ids!(
                navbar_menu.home_screen_tab,
                navbar_menu.aboutus_screen_tab,
            ))
            .selected_to_visible(
                cx,
                &self.view,
                actions,
                ids!(
                    application_pages.home_screen_frame,
                    application_pages.aboutus_screen_frame,
                ),
            );
    }
}

impl AppMain for AppWebUI {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
        if self.match_event_with_draw_2d(cx, event).is_ok() {
            return;
        }
        self.match_event(cx, event);
        self.view.handle_event(cx, event, &mut Scope::empty());
    }
}
