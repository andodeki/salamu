use makepad_widgets::Cx;

// pub mod editor;
pub mod styles;
pub mod widgets;

pub fn live_design(cx: &mut Cx) {
    styles::live_design(cx);
    // resource_imports::live_design(cx);
    widgets::live_design(cx);
    // editor::live_design(cx);
    // portal::live_design(cx);
    // modal::live_design(cx);
    // external_link::live_design(cx);
    // download_notification_popup::live_design(cx);
    // tooltip::live_design(cx);
    // desktop_buttons::live_design(cx);
}
