use makepad_widgets::*;

live_design! {
    const MODEL_LINK_FONT_COLOR = #x155EEF
    SIDEBAR_BG_COLOR = #F8F8F8

    REGULAR_FONT = {
        font_size: (12),
        font: {path: dep("crate://makepad-widgets/resources/GoNotoKurrent-Regular.ttf")}
    }

    BOLD_FONT = {
        font_size: (12),
        font: {path: dep("crate://makepad-widgets/resources/GoNotoKurrent-Bold.ttf")}
    }

    APP_NAME = {
        font_size: (12),
    }
    FONT_SIZE_H2 = 9.5
    TEXT_BIG = 12.0
    TEXT_MONO = {
        font_size: 10.0,
        font: {path: dep("crate://makepad-widgets/resources/LiberationMono-Regular.ttf")}
    }
    H2_TEXT_BOLD = {
        font_size: (FONT_SIZE_H2),
        font: {path: dep("crate://makepad-widgets/resources/IBMPlexSans-SemiBold.ttf")}
    }

    // COLOR_DOWN_2 = #x00000022
    COLOR_DOWN_2 = #xFFB084
    COLOR_DOWN_6 = #x000000CC
    COLOR_OVERLAY_BG = #x4685ff
    COLOR_HEADER_BG = #x4685ff// xFFB084 x4685ff
    SIDEBAR_BG_COLOR = #F8F8F8
    SSPACING_0 = 0.0
    SSPACING_1 = 4.0
    DROPDOWN_WIDTH = 130.0
    SSPACING_2 = (SSPACING_1 * 2)
    SPACING_0 = {top: (SSPACING_0), right: (SSPACING_0), bottom: (SSPACING_0), left: (SSPACING_0)}
    SPACING_2 = {top: (SSPACING_2), right: (SSPACING_2), bottom: (SSPACING_2), left: (SSPACING_2)}

    // ICON_CLOSE_PANEL = dep("crate://self/resources/icons/close_left_panel.svg")
    // ICON_OPEN_PANEL = dep("crate://self/resources/icons/open_left_panel.svg")


}
