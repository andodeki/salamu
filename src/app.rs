use makepad_widgets::*;

use crate::data::state::State;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import makepad_draw::shader::std::*;
    import crate::app_ui::AppUI;

    App = {{App}} {
        ui: <Root>{
            main_window = <Window>{
                window: {inner_size: vec2(800, 600)},
                body = {
                    flow: Overlay
                    width: Fill,
                    height: Fill,

                    root = <View> {
                        width: Fill,
                        height: Fill,
                        app_ui = <AppUI>{}
                    }
                }
            }
        }
    }
}

app_main!(App);

#[derive(Live, LiveHook)]
pub struct App {
    #[live]
    ui: WidgetRef,
    #[rust]
    state: State,
}

impl LiveRegister for App {
    fn live_register(cx: &mut Cx) {
        crate::makepad_widgets::live_design(cx);
        crate::app_ui::live_design(cx);
        crate::app_mobile::live_design(cx);
        crate::app_web::live_design(cx);

        crate::home::live_design(cx);
        crate::aboutus::live_design(cx);
        crate::shared::live_design(cx);
    }
}
impl MatchEvent for App {
    fn handle_actions(&mut self, _cx: &mut Cx, _actions: &Actions) {}
}

impl AppMain for App {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
        self.ui
            .handle_event(cx, event, &mut Scope::with_data(&mut self.state));
        self.match_event(cx, event);
    }
}
// impl AppMain for App {
//     fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
//         self.match_event(cx, event);
//         self.ui.handle_event(cx, event, &mut Scope::empty());
//     }
// }
