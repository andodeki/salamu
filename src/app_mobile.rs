use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import makepad_draw::shader::std::*;

    import crate::shared::styles::*;
    import crate::shared::widgets::*;
    import crate::home::home_screen::HomeScreen;
    import crate::home::app_menu::AppMenu;



    // FillerY = <View> {width: Fill}
    AppMobileUI = <ScrollXYView>{
        width: Fill,
        height: Fill,
        // padding: {top: 50, bottom: 48 }
        show_bg: false,
        flow: Down,
        spacing:10,
        align: {
            x: 0.5,
            y: 0.5
        },
        <HomeScreen>{
            app_menu = <AppMenu> {}
        }
    }
}
