use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import makepad_draw::shader::std::*;

    import crate::shared::styles::*;

    HomePanel = {{HomePanel}} {
        flow: Overlay
        width: Fill
        height: Fill

        <View> {
            flow: Overlay,
            width: Fill
            height: Fill
            padding: {left: 25, right: 25, bottom: 20},

            <View>{
                flow: Down,
                spacing:10,
                align: {
                    x: 0.5,
                    y: 0.5
                },

                button1 = <Button> {
                    text: "Hello world "
                    draw_text:{color:#f00}
                }
                input1 = <TextInput> {
                    width: 100, height: 30
                    text: "Click to count "
                }
                <View>{
                    scroll_bars: <ScrollBars> {show_scroll_x: false, show_scroll_y: true}
                    label1 = <Label> {
                        draw_text: {
                            color: #f
                        },
                        text: r#"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique condimentum tristique. Donec sapien arcu, molestie vitae neque pretium, ultrices luctus diam. Aenean a eros ac lectus sollicitudin eleifend non in tellus. Nullam sapien velit, sodales et tincidunt vestibulum, sollicitudin et purus. Praesent elementum risus rhoncus enim consectetur pulvinar. Quisque rutrum leo quis odio mattis blandit. Etiam sit amet nibh felis. Vivamus maximus hendrerit turpis, vitae efficitur risus faucibus in. Vestibulum lorem dui, consectetur consectetur magna nec, hendrerit bibendum magna. Mauris faucibus rhoncus turpis luctus porta. Aenean interdum auctor sapien ac hendrerit.

                            Aliquam erat volutpat. Praesent velit felis, iaculis at interdum sed, pellentesque nec tortor. Nulla mauris augue, sollicitudin non nisi ac, consequat dapibus lorem. Maecenas mollis, nulla id tincidunt finibus, neque enim ultricies libero, vel accumsan metus libero vel mauris. Vivamus et suscipit nisl, vel lacinia massa. Sed et bibendum lectus, nec pellentesque tortor. Cras non est ut eros venenatis volutpat quis quis risus. Suspendisse convallis vestibulum orci. Etiam sit amet nisl eleifend, semper nibh sit amet, tincidunt leo. Sed ut tristique nunc. Nulla dictum hendrerit augue.

                            Vivamus ac porttitor sem. In auctor posuere velit ac molestie. Suspendisse ornare ex quis eros porttitor tincidunt. Praesent tincidunt purus tellus, vel malesuada dui condimentum at. Morbi pellentesque, velit euismod tristique rhoncus, metus mi tincidunt lacus, at faucibus tortor nunc ut nibh. Etiam efficitur est diam, ut commodo enim bibendum at. Suspendisse accumsan gravida nisi, sit amet sodales lectus maximus eu."#,
                        // width: 400.0,
                        width: Fill,
                    }
                }


            }

            <View>{
                footer = <Label> {
                    margin: {bottom: (SSPACING_1)}
                    draw_text: {
                        text_style: <H2_TEXT_BOLD> {},
                        color: #000
                    }
                    text: "Here"
                }
            }
        }
    }
}

#[derive(Live, LiveHook, Widget)]
pub struct HomePanel {
    #[deref]
    view: View,

    // #[rust]
    // state: State,
    // #[rust]
    // portal_list_end_reached: bool,
    #[rust]
    counter: usize,
}

impl Widget for HomePanel {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.view.handle_event(cx, event, scope);
        self.widget_match_event(cx, event, scope);
        // self.match_event(cx, event);
        // self.view.handle_event(cx, event, &mut Scope::empty());
    }
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // DrawStep::done()
        self.view.draw_walk(cx, scope, walk)
    }
}

impl WidgetMatchEvent for HomePanel {
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, _scope: &mut Scope) {
        if self.view.button(id!(button1)).clicked(&actions) {
            log!("Press button {}", self.counter);
            self.counter += 1;
            let label = self.view.label(id!(label1));
            label.set_text_and_redraw(cx, &format!("Counter: {}", self.counter));
        }
    }
}

// impl WidgetMatchEvent for ChatPanel {
//     fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, scope: &mut Scope) {}
// }
