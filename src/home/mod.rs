pub mod app_menu;
pub mod home_panel;
pub mod home_screen;

use makepad_widgets::Cx;

pub fn live_design(cx: &mut Cx) {
    app_menu::live_design(cx);
    home_screen::live_design(cx);
    home_panel::live_design(cx);
}
