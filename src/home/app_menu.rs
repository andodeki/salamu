use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import makepad_draw::shader::std::*;

    import crate::shared::styles::*;
    import crate::shared::widgets::*;
    import makepad_draw::shader::std::*;

    ICON_CLOSE_PANEL = dep("crate://self/resources/icons/close_right_panel.svg")
    ICON_OPEN_PANEL = dep("crate://self/resources/icons/open_right_panel.svg")

    AppMenu = {{AppMenu}} <MoxinTogglePanel> {
        open_content = {
            <View> {
                width: Fill
                height: Fill
                padding: {top: 70, left: 25.0, right: 25.0}
                spacing: 35
                flow: Down
                show_bg: true
                draw_bg: {
                    color: #F2F4F7
                }

                label = <Label> {
                    draw_text: {
                        text_style: <BOLD_FONT>{font_size: 12}
                        color: #667085
                    }
                    text: "Salamu"
                }
            }
        }
        persistent_content = {
            default = {
                before = {
                    width: Fill
                }
                open = {
                    draw_icon: {
                        svg_file: (ICON_OPEN_PANEL),
                    }
                }
                close = {
                    draw_icon: {
                        svg_file: (ICON_CLOSE_PANEL),
                    }
                }
            }
        }
    }
}

#[derive(Live, LiveHook, Widget)]
pub struct AppMenu {
    #[deref]
    deref: TogglePanel,
    // #[rust]
    // current_chat_id: Option<ChatID>,
}

impl Widget for AppMenu {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.deref.handle_event(cx, event, scope);
        self.widget_match_event(cx, event, scope);

        // TODO This is a hack to redraw the chat history and reflect the
        // name change on the first message sent.
        // Maybe we should send and receive an action here?
        if let Event::Signal = event {
            self.redraw(cx);
        }
    }
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // self.deref.draw_walk(cx, scope, walk)
        self.deref.draw_walk(cx, scope, walk)
        // DrawStep::done()
    }
}

impl WidgetMatchEvent for AppMenu {
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, _scope: &mut Scope) {
        let close = self.button(id!(close_panel_button));
        let open = self.button(id!(open_panel_button));

        if close.clicked(&actions) {
            close.set_visible(false);
            open.set_visible(true);
            self.set_open(cx, false);
        }

        if open.clicked(&actions) {
            open.set_visible(false);
            close.set_visible(true);
            self.set_open(cx, true);
        }

        // if self.button(id!(close_panel_button)).clicked(&actions) {
        //     self.button(id!(close_panel_button)).set_visible(false);
        //     self.button(id!(open_panel_button)).set_visible(true);
        //     self.deref.set_open(cx, false);
        // }

        // if self.button(id!(open_panel_button)).clicked(&actions) {
        //     self.button(id!(open_panel_button)).set_visible(false);
        //     self.button(id!(close_panel_button)).set_visible(true);
        //     self.deref.set_open(cx, true);
        // }
    }
}
