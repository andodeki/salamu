use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import makepad_draw::shader::std::*;

    import crate::shared::styles::*;
    import crate::home::home_panel::HomePanel;

    HomeScreen = {{HomeScreen}} {
        width: Fill,
        height: Fill,
        spacing: 10.0,

        <View> {
            debug: A

            <View> {
                width: Fill,
                height: Fill,
                align: {x: 0.5},
                padding: {top: 50, bottom: 48 }
                // debug: A
                home_panel = <HomePanel> {}
            }
            <View> {
                width: Fit,
                height: Fill,
                // debug: A
                app_menu = <View>{}
            }
        }
    }
}

#[derive(Live, LiveHook, Widget)]
pub struct HomeScreen {
    #[deref]
    view: View,
}

impl Widget for HomeScreen {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        // TODO This check is actually copied from Makepad view.rs file
        // It's not clear why it's needed here, but without this line
        // the "View all files" link in Discover section does not work after visiting the chat screen

        if self.visible || !event.requires_visibility() {
            self.view.handle_event(cx, event, scope);
        }
    }

    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        self.view.draw_walk(cx, scope, walk)
    }
}
