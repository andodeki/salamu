use std::path::Path;

use makepad_widgets::*;

use crate::data::state::State;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;
    import crate::app_web::AppWebUI;
    import crate::app_mobile::AppMobileUI;

    import crate::home::home_screen::HomeScreen;
    import crate::aboutus::about_us::AboutUs;


    import crate::shared::widgets::NavbarMenuButton;

    ICON_DISCOVER = dep("crate://self/resources/icons/discover.svg")
    ICON_CHAT = dep("crate://self/resources/icons/chat.svg")
    ICON_MY_MODELS = dep("crate://self/resources/icons/my_models.svg")

    AppUI = {{AppUI}} {
        width: Fill,
        height: Fill,
        align: {x: 0.5},
        padding: {top: 0, bottom: 48 }
        // debug: A

        <View> {
            width: Fill,
            height: Fill,
            mobile = <AppMobileUI>{}
            web = <AppWebUI>{}
        }
    }
}

enum ScreenSize {
    Mobile,
    Web,
}

#[derive(Live, Widget)]
pub struct AppUI {
    #[deref]
    view: View,

    #[rust]
    state: State,
    // #[rust]
    // screen_width: f64,
}

impl LiveHook for AppUI {
    fn after_new_from_doc(&mut self, cx: &mut Cx) {
        let mobile_screen = self.view.view(id!(mobile));
        let web_screen = self.view.view(id!(web));
        if self.state.screen_width < 960_f64 {
            log!("SCREEN_WIDTH ===> {}", self.state.screen_width);
            web_screen.set_visible(false);
            mobile_screen.set_visible_and_redraw(cx, true);
            // web_screen.draw_all(cx, &mut Scope::empty());
        } else if self.state.screen_width > 960_f64 {
            mobile_screen.set_visible(false);
            web_screen.set_visible_and_redraw(cx, true);
            // web_screen.draw_all(cx, &mut Scope::empty());
        } else {
            log!("SCREEN_WIDTH ===> {}", self.state.screen_width);
        }
        // Image directory
        let home = std::env::var("HOME")
            .or_else(|_| std::env::var("USERPROFILE"))
            .expect("home not found");

        self.state.load_images(&Path::new(&home).join("Downloads"));
    }
    // fn handle_startup(&mut self, _cx: &mut Cx) {
    //     let home = std::env::var("HOME")
    //         .or_else(|_| std::env::var("USERPROFILE"))
    //         .expect("home not found");

    //     self.state.load_images(&Path::new(&home).join("Downloads"));
    // }
}

impl Widget for AppUI {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.view
            .handle_event(cx, event, &mut Scope::with_data(&mut self.state));
        // self.view.handle_event(cx, event, scope);
        self.widget_match_event(cx, event, scope);
    }
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // DrawStep::done()
        // let state = scope.data.get::<State>().unwrap();
        let mobile_screen = self.view.view(id!(mobile));
        let web_screen = self.view.view(id!(web));

        while let Some(_next) = self.view.draw(cx, scope).step() {
            // if let Some(mut mobile_screen) = mobile_screen.has_widget(&next).borrow_mut() {}
            if self.state.screen_width == 0_f64 {
                continue;
            }
            // if self.state.screen_width < 960_f64 {
            if self.state.screen_width < 960_f64 {
                web_screen.set_visible(false);
                mobile_screen.set_visible_and_redraw(cx, true);
                mobile_screen.draw_all(cx, scope);
                self.view.draw_walk(cx, scope, walk)?;
            } else if self.state.screen_width > 960_f64 {
                mobile_screen.set_visible(false);
                web_screen.set_visible_and_redraw(cx, true);
                web_screen.draw_all(cx, scope);
                self.view.draw_walk(cx, scope, walk)?;
            } else {
                log!("ESLE_SCREEN_WIDTH ===> {}", self.state.screen_width);
            }
        }
        DrawStep::done()
    }
}

impl WidgetMatchEvent for AppUI {
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, scope: &mut Scope) {
        let state = scope.data.get_mut::<State>().unwrap();
        for action in actions {
            if let WindowAction::WindowGeomChange(ce) = action.as_widget_action().cast() {
                state.screen_width = ce.new_geom.inner_size.x * ce.new_geom.dpi_factor;
                let mobile_screen = self.view.view(id!(mobile));
                let web_screen = self.view.view(id!(web));
                if state.screen_width < 960_f64 {
                    web_screen.set_visible(false);
                    mobile_screen.set_visible_and_redraw(cx, true);
                } else if state.screen_width > 960_f64 {
                    mobile_screen.set_visible(false);
                    web_screen.set_visible_and_redraw(cx, true);
                } else {
                    log!("ELSE_SCREEN_WIDTH ===> {}", state.screen_width);
                }
            }
        }
    }
}
