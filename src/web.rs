use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;
    import crate::home::home_screen::HomeScreen;
    import crate::aboutus::about_us::AboutUs;

    import crate::shared::widgets::NavbarMenuButton;

    ICON_DISCOVER = dep("crate://self/resources/icons/discover.svg")
    ICON_CHAT = dep("crate://self/resources/icons/chat.svg")
    ICON_MY_MODELS = dep("crate://self/resources/icons/my_models.svg")

    AppWebUI = <ScrollXYView>{
        width: Fill,
        height: Fill,
        align: {x: 0.5},
        padding: {top: 0, bottom: 48 }
        // debug: A

        navbar_menu = <RoundedView> {
            width: Fill,
            height: Fit,
            flow: Right, spacing: 20.0,
            padding: { top: 0 }
            align: {x: 0.5, y: 0.0},

            show_bg: true,
            draw_bg: {
                color: (SIDEBAR_BG_COLOR),
                instance radius: 0.0,
                border_color: #EAECF0,
                border_width: 1.2,
            }
            home_screen_tab = <NavbarMenuButton> {
                animator: {selected = {default: on}}
                text: "Home",
                draw_icon: {
                    svg_file: (ICON_DISCOVER),
                }
            }
            aboutus_screen_tab = <NavbarMenuButton> {
                text: "About Us",
                draw_icon: {
                    svg_file: (ICON_MY_MODELS),
                }
            }
            application_pages = <View> {
                margin: 0.0,
                padding: 0.0,
                flow: Down,
                width: Fill,
                height: Fill,
                // debug: A

                home_screen_frame = <HomeScreen> {visible: true}
                aboutus_screen_frame = <AboutUs> {visible: false}
            }
        }
    }
}
#[derive(Live, Widget)]
pub struct AppWebUI {
    #[deref]
    view: Window,
    // #[live]
    // view: WidgetRef,
}
impl LiveHook for AppWebUI {
    fn after_new_from_doc(&mut self, _cx: &mut Cx) {
        println!("after_new_from_doc(): starting some kind of a loop");
    }
}

impl Widget for AppWebUI {
    fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
        self.view.handle_event(cx, event, scope);
        self.widget_match_event(cx, event, scope);
    }
    // fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
    //     self.match_event(cx, event);
    //     self.view.handle_event(cx, event, &mut Scope::empty());
    // }
    fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
        // DrawStep::done()
        self.view.draw_walk(cx, scope, walk)
    }
}

impl WidgetMatchEvent for AppWebUI {
    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, scope: &mut Scope) {
        self.view
            .radio_button_set(ids!(
                navbar_menu.home_screen_tab,
                navbar_menu.aboutus_screen_tab,
            ))
            .selected_to_visible(
                cx,
                &self.view,
                actions,
                ids!(
                    application_pages.home_screen_frame,
                    application_pages.aboutus_screen_frame,
                ),
            );
    }
}
/*
error[E0308]: mismatched types
   --> src/app_web.rs:102:17
    |
100 |             .selected_to_visible(
    |              ------------------- arguments to this method are incorrect
101 |                 cx,
102 |                 &self.view,
    |                 ^^^^^^^^^^ expected `&WidgetRef`, found `&Window`
    |
    = note: expected reference `&makepad_widgets::WidgetRef`
               found reference `&makepad_widgets::Window`
note: method defined here
   --> /Users/aok/Projects/rustdev/MadeByMakepad/makepad/widgets/src/radio_button.rs:202:12
    |
202 |     pub fn selected_to_visible(&self, cx: &mut Cx, ui:&WidgetRef, actions: &Actions, paths:&[&[Liv...
    |            ^^^^^^^^^^^^^^^^^^^
*/
